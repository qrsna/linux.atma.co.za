# Linux Wiki 
## Development Environment and Live Site

https://linux.atma.co.za

## Setup

#### Build docker images locally

`cd base-image && docker build -t infinitetutts/alpine-python:3.5.3 .` 

`cd ../ && docker build -t infinitetutts/mkdocs:latest .`

#### Start server
`./docker.sh start`

#### Edit website files with your IDE
`./volume/docs/*`

`./volume/mkdocs.yml`


## Upload to live site

#### Build website (Builds static site)
`./docker.sh build`

#### Upload
`git push`
