#!/bin/bash

if [ "$1" == "new" ] # Create a new project [ Deprecated ]
then
  docker run --rm -it --name mkdocs -p 8000:8000 -v $(pwd)/volume/:/usr/share/mkdocs infinitetutts/mkdocs:latest new 
elif [ "$1" == "start" ] # Start the live-reloading docs server
then
  docker run --rm -it --name mkdocs -p 8000:8000 -v $(pwd)/volume/:/usr/share/mkdocs/ infinitetutts/mkdocs:latest
  elif [ "$1" == "build" ] # Build the site
then
  echo "Building site"
  docker run --rm -it --name mkdocs -v $(pwd)/volume/:/usr/share/mkdocs/ infinitetutts/mkdocs:latest build
  echo "Deleting old gitlab pages site"
  rm -r $(pwd)/public/*
  echo "Copying new site for gitlab pages"
  cp -r $(pwd)/volume/site/* $(pwd)/public/
else 
    echo "Usage: ./docker.sh <new> or <start> or <build>"
fi
