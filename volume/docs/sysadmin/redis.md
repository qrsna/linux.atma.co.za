**Connect to redis host**
```
redis-cli -h 192.168.1.1
```

**Commands**

`info`      – Displays ALL info, (Starting point)   
`info stats`      – Displays select info    
`config get maxmemory-policy`   
`config set maxmemory-policy volatile-lru`  
`config get maxmemory`  
`config set maxmemory 20000000000`  
`config get \*`      – Displays all configurable variables 
`slowlog get 25`      – Displays the top 25 slow queries  
`resetstat`      –  Resets stats (Not all)  
`help config resetstat`      – Explains Command 


**Watch Command**
```
watch redis-cli -h 192.168.1.1 info stats
```

**Eviction policies**   

The exact behavior Redis follows when the maxmemory limit is reached is configured using the maxmemory-policyconfiguration directive.

The following policies are available:   
`noeviction`: return errors when the memory limit was reached and the client is trying to execute commands that could result in more memory to be used (most write commands, but DEL and a few more exceptions).    
`allkeys-lru`: evict keys trying to remove the less recently used (LRU) keys first, in order to make space for the new data added.    
`volatile-lru`: evict keys trying to remove the less recently used (LRU) keys first, but only among keys that have anexpire set, in order to make space for the new data added.  
`allkeys-random`: evict random keys in order to make space for the new data added.  
`volatile-random`: evict random keys in order to make space for the new data added, but only evict keys with anexpire set.  
`volatile-ttl`: In order to make space for the new data, evict only keys with an expire set, and try to evict keys with a shorter time to live (TTL) first. 
