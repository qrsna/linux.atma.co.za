**General log messages**
```
/var/log/messages
```

**System boot log**
```
/var/log/boot
```

**Debugging log messages**
```
/var/log/debug
```

**User login and authentication logs**
```
/var/log/auth.log
```

**Running services **
```
/var/log/daemon.log
```

**Linux kernel ring buffer log**
```
/var/log/dmesg
```

**All binary package logs **
```
/var/log/dpkg.log
```

**User failed login log file**
```
/var/log/faillog
```

**Kernel log file**
```
/var/log/kern.log
```

**Printer log file**
```
/var/log/lpr.log
```

**All userlevel logs**
```
/var/log/user.log
```

**X.org log file**
```
/var/log/xorg.0.log
```

**fsck command log**
```
/var/log/fsck/*
```

**Application crash report & log file**
```
/var/log/apport.log
```
