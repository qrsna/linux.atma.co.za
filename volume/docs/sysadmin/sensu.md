## Sensu

**Clear dashboard**
```
sensu-cli event list --format json | jq --raw-output "map(select( .[\"check\"][\"issued\"]  )) | .[] | .client.name + \" \" +  .check.name " | xargs --verbose --no-run-if-empty -n2 sensu-cli resolve 
```
