## Runlevels

A `runlevel` represents the current state of a Linux system.
```text
Runlevel 0: System shutdown
Runlevel 1: Single-user, rescue mode
Runlevels 2, 3, 4: Multi-user, text mode with networking enabled
Runlevel 5: Multi-user, network enabled, graphical mode
Runlevel 6: System reboot
```

## Redhat / Centos

**Create a startup script**
```bash
sudo vi /etc/init.d/script.sh 
sudo chmod 755 /etc/init.d/script.sh
```

**Set script to start automatically on boot with**
Centos
```bash
sudo chkconfig --add script.sh 
```


**Enable script on runlevels**
```bash
sudo chkconfig --level 2345 script.sh on 
```

**Check if the script is enabled - you should see `on` for the levels you selected.**
```bash
sudo chkconfig --list | grep script.sh 
```

**Remove script from startup** 
```bash
chkconfig --del script.sh
```

## Ubuntu

**Create a startup script**
```bash
sudo vi /etc/init.d/script.sh 
sudo chmod 755 /etc/init.d/script.sh
```

**Add a script to startup** 

`defaults` selects all the default runlevels
```bash
sudo update-rc.d script.sh defaults 
```
**Remove script from startup**
```bash
update-rc.d -f script.sh remove 
```
**Check if the script is enabled**
```bash
service --status-all 
```
`[ + ]` Services with this sign are currently running.  
`[ – ]` Services with this sign are not currently running.   
`[ ? ]` Services that do not have a status switch. 


