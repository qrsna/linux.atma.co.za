**Run playbook**
```
ansible-playbook -i inventory/foo/hosts -l webservers -u ubuntu -S install_sensu.yml 
```     
`-i` = inventory file, `-l` = server group, `-u` = ssh user, `-S` = playbook

**Send ping to all servers within host file**
```
ansible all -m ping -i inventory 
```


**Check what hosts will be affected**
```
ansible-playbook site.yml -i inventory --list-hosts
```


**Install the dependancy only on a host group to run a dry run:**
```
ansible -i inventory dbCluster -m apt -a "name=python-apt state=present --become"
```

**Dry run**
```
ansible-playbook site.yml -i inventory --check
```
