**Delete logs**
```
20 0 * * * /usr/local/bin/curator --host 127.0.0.1 delete indices --older-than 30 --time-unit days --timestring '%Y.%m.%d'
```

**Get indices and shard info**
```
curl -XGET "http://localhost:9200/_cat/shards?v"
curl -XGET "http://localhost:9200/_cat/indices"
```

**Get indexes**
```
curl -XGET 'http://localhost:9200/filebeat*/_aliases?pretty'
```

**Get Index Sizes**
```
curl localhost:9200/index1,index2/_stats
curl localhost:9200/filebeat-2016.07.13/_stats
```

**Delete Indexes**
```
curl -XDELETE 'http://localhost:9200/twitter/'
```

**Node Stats**
```
curl -XGET "http://localhost:9200/_nodes/stats?v" | jq .
```

**Cluster info**
```
curl -XGET 'http://localhost:9200/_cluster/health?pretty'
curl -XGET 'http://localhost:9200/_cluster/state'
```

**Node Info**
```
curl -XGET 'http://localhost:9200/_nodes'
curl -XGET 'http://localhost:9200/_nodes/stats'
```

**Filebeat-2016.07.13 (jq displays in json format)**
```
curl localhost:9200/filebeat-2016.07.13,filebeat-2016.07.12/_stats | jq .
```
