## CUT
**Displayfirst field after delimenator**
```bash
cut -d':' -f1 /etc/passwd
```
Prints the usernames from `/etc/passwd`


## TAIL
**Delete first 4 lines and last 4 lines from a text file**
```bash
tail -n +5 srv_ssh.json | head -n -4 > test.json.new && mv test.json.new srv_ssh.json
```

## Sort
**Remove duplicate lines**
```bash
sort file.log | uniq > newfile.log
```

## GREP
**Display files that do not have string:**
```bash
grep -L "foo" *
```

**Display files that do have string**
```bash
grep -n "foo" *
```

**Displays unique values from compared files**
```bash
grep -v -f <compare-file-1> <compare-file-2> 
```
