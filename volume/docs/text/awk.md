**Removing quotes**
```bash
awk '{ print "\""$0"\""}' inputfile
```
If your file has empty lines, awk is definitely the way to go
```bash
awk 'NF { print "\""$0"\""}' inputfile.txt > newfile.txt
```
`NF` tells awk to only execute the print command when the Number of Fields is more than zero (line is not empty).


**Print 3rd field**
```bash
awk '{print $3}' maps.txt 
```

**Sort strings by length**
```bash
awk '{ print length($0),$0 | "sort -n"}'  maps.txt
```

**Examples**  

AWK's `printf`, `NR` and `$0` make it easy to have precise and flexible control over the formatting

maplist.txt
```
surf_004_final1
surf_1day
surf_2012_beta12
```

**1**  
```bash
awk '{printf("%010d %s\n", NR, $0)}' maplist.txt
```
Output:    
```
0000000001 surf_004_final1
0000000002 surf_1day
0000000003 surf_map3
```

mapcycle_surf1.txt
```
"surf_004_final1"
"surf_1day"
"surf_2012_beta12"
```

**2**  
```bash
awk '{printf("%d %s \n", NR, $0)}' mapcycle_surf1.txt 
```
Output:  
```
1 "surf_004_final1"
2 "surf_1day"   
3 "surf_2012_beta12"
```

**3**  
```bash
awk '{printf("%s %d \n" ,$0 , NR)}' mapcycle_surf1.txt 
```
Output:   
```
"surf_004_final1" 1
"surf_1day" 2
"surf_2012_beta12" 3
```

**4**  
```bash
awk '{printf("%s \"%d\" \n" ,$0 , NR)}' mapcycle_surf1.txt 
```
Output:    
```
"surf_004_final1" "1"
"surf_1day" "2"
"surf_2012_beta12" "3"
```

**5**  
```bash
awk '{printf("%s \"%d\" \n" ,$0 , NR-1)}' mapcycle_surf1.txt > mapcycle_surf.txt 
```
Output:    
```
"surf_004_final1" "0"
"surf_1day" "1"
"surf_2012_beta12" "2"
"surf_3" "3"
```
