## System
**Display virtual memory statistics**
```
vmstat 1
```

**Display I/O statistics**
```
iostat 1
```

**List files opened by user**
```
lsof -u user
```

**View memory usage of processes**
```
ps -eo size,pid,user,command --sort -size | awk '{ hr=$1/1024 ; printf("%13.2f Mb ",hr) } { for ( x=4 ; x<=NF ; x++ ) { printf("%s ",$x) } print "" }' |cut -d "" -f2 | cut -d "-" -f1
```

## Network
**Find port of a running application**
```bash
sudo netstat -lnp | grep ts3
sudo netstat -lpna
```

**Find an appliction listening on a port**
```bash
sudo netstat -lntp | grep ':80'
sudo lsof -i :80
```

**Monitor live network connections and traffic**
```bash
sudo tcptrack -i eth0 -r 5 
```

**Capture and display all packets on interface eth0**
```
tcpdump -i eth0
```

**Monitor all traffic on port 80**
```
tcpdump -i eth0 'port 80'
```
