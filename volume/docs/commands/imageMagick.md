*ImageMagick - Command Line Image Manipulation*

**Installation**
```bash
sudo apt-get install imagemagick 
sudo yum -y install ImageMagick 
```

**Converting Between Formats**
```bash
convert howtogeek.png howtogeek.jpg 
```

**Resize Images**
```bash
convert example.png -resize 200×100 example.png
```

**Resize Images & Preserve aspect ratio**
```bash
convert example.png -resize 200×100! example.png 
```

**Rotate Image**
```bash
convert howtogeek.jpg -rotate 90 howtogeek-rotated.jpg 
```

**Converting Between Formats & Changing quality**
```bash
convert howtogeek.png -quality 95 howtogeek.jpg 
```
The Last example shows you that you can combined operations.
