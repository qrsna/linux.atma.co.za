*Listing User Logins, reboot times and bad login attempts*

**Get the running processes of logged-in users**
```bash
w
```

**Display Logged in users**
```bash
users 

who | cut -d' ' -f1 | sort | uniq
```

**Get all users login and logout history**
```bash
last 
```

**Get the users login history**
```bash
last [user] 
```

**Display bad login attempts**
```bash
last b 
```     

**Display last reboot times**
```bash
last reboot | less 
```
