# Tmux

**Start new with session name**
```bash
tmux new -s myname
```

**Attach**
```bash
tmux a tmuxname
```

**Attach to named**
```bash
tmux a -t myname
```

**List sessions**
```bash
tmux ls
```

**Kill session**
```bash
tmux kill-session -t myname
```

# Screen
**Inject a command to a detached screen**
```bash
screen -p 0 -X stuff $'bot_kill\n'
```
